'use strict';

function createNewUser(userName, userSurName, userDate) {
    userName = prompt('Введіть ваше ім\'я');
    userSurName = prompt('Введіть ваше прізвище');
    userDate = prompt('Введіть дату вашого народження', 'dd.mm.yyyy');
    const newUser = {
        firstName: userName,
        lastName: userSurName,
        birthday: userDate,
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        getAge() {
            const day = parseInt(this.birthday.substring(0, 2));
            const month = parseInt(this.birthday.substring(3, 5));
            const year = parseInt(this.birthday.substring(6, 10));
            const today = new Date();
            const birthDate = new Date(year, month - 1, day);
            let age = today.getFullYear() - birthDate.getFullYear();
            const m = today.getMonth() - birthDate.getMonth();
            if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                age--;
            }
            return age;
        },
        getPassword() {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.substring(6);
        },
    };

    return newUser;
};
const user = createNewUser();

console.log(user);
console.log(user.getAge());
console.log(user.getPassword());
