## Теоретичні питання

1.Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
2.Які засоби оголошення функцій ви знаєте?
3.Що таке hoisting, як він працює для змінних та функцій?

### Відповідь 1.

**Екрануванням** називають процес додавання спецсимволів перед символами або рядками, екранування в програмуванні потрібне для того, щоб запобігти їх неправильному тлумаченню.

### Відповідь 2.

Спосіб **Function declaration**.
Оголошення функції за допомогою ключового слова _function_ та імені функції.

Спосіб **Function expression**.
Оголошення функції за допомогою ключового слова _function_ та записанням функції у змінну.

Спосіб **Named Function expression**.
Оголошення функції за допомогою ключового слова _function_, імені функції та записанням функції у змінну. Тільки таку функцію не можна буде викликати по цьому імені.

### Відповідь 3.
hoisting(підняття) — це поведінка JavaScript за замовчуванням, коли всі оголошення переміщуються у верхню частину поточної області.

Змінні можуть бути ініціалізованими та використані до того, як вони будуть оголошені, але їх не можна використовувати без ініціалізації.

Функції можна викликати до того, як вони будуть об'явлені.
